{ stdenv, autoconf, gcc }:

stdenv.mkDerivation rec {
  src = ./.;
  name = "quick-multi-paranoid-${version}";
  version = "1.3.5-nix"; # matches the sonicparanoid version
  nativeBuildInputs = [ autoconf gcc ];
  buildPhase = ''
    make qa
  '';
  installPhase = ''
    mkdir -p $out/bin
    cp qp qa1 qa2 $out/bin
  '';
}
